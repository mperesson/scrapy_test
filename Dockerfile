FROM python:3.6.8-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache curl python3 pkgconfig python3-dev openssl-dev libffi-dev libxml2-dev libxslt-dev musl-dev make gcc

COPY . /app/
RUN pip install -r /app/requirements.txt
WORKDIR /app

CMD ["python", "/app/script.py"]
