from bs4_version import get_trustpilot_review
from scrapy_version import run_spider
import sys

if len(sys.argv) != 2 or sys.argv[1] not in ['bs4', 'scrapy']:
    raise(Exception('Param required: bs4 / scrapy'))

if sys.argv[1] == 'bs4':
    get_trustpilot_review()
else:
    run_spider()

