import scrapy
from scrapy.crawler import CrawlerProcess
import urllib
import json

languages = [
    'en',
    'es'
]

rating = [
    5, 4, 3, 2, 1
]

class TrustPilotSpider(scrapy.Spider):
    name = 'trustpilotspider'
    BASE_URL = 'https://www.trustpilot.com/review/www.google.com'

    def start_requests(self):
        requests = []
        for lg in languages:
            for star in rating:
                params = {
                    'languages': lg,
                    'stars': star,
                }
                url = f'{self.BASE_URL}?{urllib.parse.urlencode(params)}'
                request = scrapy.Request(url, callback=self.parse)
                request.meta['params'] = params
                requests.append(request)
        return requests

    def parse(self, response):
        for article in response.css('article'):
            date_data = article.css('script[data-initial-state="review-dates"]::text').get()
            date_data = json.loads(date_data)
            review_title = article.css('.review-content__title')

            yield {
                'id': article.attrib['id'],
                'username': article.css('.consumer-information__name::text').get(),
                'title': review_title.css('a::text').get(),
                'comment': article.css('.review-content__text::text').get(),
                'date': date_data['publishedDate'],
                'language': response.meta['params']['languages'],
                'rating': response.meta['params']['stars'],
            }


def run_spider():
    process = CrawlerProcess(settings={
        'FEED_FORMAT': 'json',
        'FEED_URI': 'items.json'
    })

    process.crawl(TrustPilotSpider)
    process.start()  # the script will block here until the crawling is finished
